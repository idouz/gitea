package Servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/hello")
public class HelloServlet extends MyHttpServlet  {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
       String id = request.getParameter("id");


       response.getWriter().write("helloGet");

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
       response.getWriter().write("helloPost");
    }
}
