package Servlet;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@WebServlet("/upload")
public class uploadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     //方法1

        //         //输入流获得客户端传来的数据流
//        InputStream inputStream =  req.getInputStream();
//        Reader reader  = new InputStreamReader(inputStream);
//        BufferedReader bufferedReader = new BufferedReader(reader);
//        //通过输出流将数据流输出到本地硬盘
//
//        //获取文件夹的绝对路径
//        String path = req.getServletContext().getRealPath("file/copy.txt");
//        OutputStream outputStream = new FileOutputStream(path);
//        Writer writer = new OutputStreamWriter(outputStream);
//        BufferedWriter bufferedWriter = new BufferedWriter(writer);
//        String str = "";
//        while ((str = bufferedReader.readLine())!=null){
//           bufferedWriter.write(str);
//        }
//        bufferedReader.close();
//        bufferedWriter.close();
//        writer.close();
//        outputStream.close();
//        reader.close();
//        inputStream.close();

     //方法2 引入jar包使用方法

        try {
            DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory();
            ServletFileUpload servletFileUpload = new ServletFileUpload(fileItemFactory);
            //用一个list集合存取
            List<FileItem> list = servletFileUpload.parseRequest(req);
            for (FileItem fileItem: list) {

                if (fileItem.isFormField()) {
                    String name = fileItem.getFieldName();
                    String value = fileItem.getString("UTF-8");
                    System.out.println(name + ":" + value);
                } else {
                    String filename = fileItem.getName();
                    InputStream inputStream = fileItem.getInputStream();
                    //获取文件的绝对路径
                    String path = req.getServletContext().getRealPath("file/" + filename);
                    OutputStream outputStream = new FileOutputStream(path);
                    int temp = 0;
                    while (( temp = inputStream.read())!= -1) {
                        outputStream.write(temp);

                    }
                    outputStream.close();
                    inputStream.close();
                    System.out.println("上传成功");

                }
            }
        } catch (FileUploadException e) {
            e.printStackTrace();
        }

    }
}
