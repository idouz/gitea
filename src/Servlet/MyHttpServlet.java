package Servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/MyServlet1")
public class MyHttpServlet extends MyGenericServlet{
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        HttpServletRequest request  = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        String method = request.getMethod();
        switch (method){
            case "Get":
                this.doGet(request,response);
                break;
            case "Post":
                this.doPost(request,response);
                break;
        }


    }
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException {

    }
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException {

    }
}
