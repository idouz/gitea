package Servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@WebServlet("/download")
public class downloadServlet extends HttpServlet {



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("type");
        String filename ="";
        switch (type){
            case "png":
                filename="1.png";
                break;
            case "txt":
                filename ="1.txt";
                break;
        }
        //设置响应的格式
        resp.setContentType("application/x-msdownload");

        //设置下载之后的文件名
        resp.setHeader("Content-Disposition","attachment;filename="+filename);
        //获取输出流
        OutputStream outputStream = resp.getOutputStream();
        //获取服务器下载的绝对路径
        String path = req.getServletContext().getRealPath("file/"+filename);
        InputStream inputStream = new FileInputStream(path);
        int temp = 0;
        while ((temp = inputStream.read())!=-1){
            outputStream.write(temp);
        }
        inputStream.close();
        outputStream.close();
    }
}
